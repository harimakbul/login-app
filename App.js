import React from 'react';
import SplashScreen from './SplashScreen';
import Route from './Route'

class App extends React.Component {
  constructor () {
    super()
    global.server = 'http://192.168.43.80:3333/api/v1'
  }
  state = {
    view : <SplashScreen />
  }
  componentDidMount() {
    setTimeout(() => {
      this.setState({
        view : <Route />
      })
    }, 3000)
  }
  render() {
    return (
      <>
        {this.state.view}
      </>
    )
  }
};

export default App;
