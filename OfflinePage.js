import React from 'react'
import {
  Text,
  Image,
  TouchableOpacity,
  Linking,
  StatusBar
} from 'react-native'
import { 
  View,
  Spinner
} from 'native-base'
import NetInfo from "@react-native-community/netinfo"
import { Actions } from 'react-native-router-flux'

class OfflinePage extends React.Component {
  state = {
    loading : false
  }
  CheckConnectivity = () => {
    this.setState({
      loading : true
    })
    const unsubscribe = NetInfo.addEventListener(state => {
      if(state.isConnected !== true) {
        setTimeout(() => {
          this.setState({
            loading : false
          })
        }, 500)
      } else {
        setTimeout(() => {
          this.setState({
            loading : false
          })
          Actions.login()
        }, 500)
      }
    })
    unsubscribe()
  }
  render() {
    return (
      <>
        {this.state.loading ? (
          <Spinner style={{position: 'absolute', top : -10, left : 10}} color="#288EDB" />
        ) : (
          <></>
        )}
        <StatusBar backgroundColor="#288EDB" barStyle="light-content" />
        <View style={{alignItems : 'center', justifyContent : 'center', alignContent : 'center', flex : 1, width : '100%'}}>
          <Image source={require('./Assets/images/error-connection.png')} style={{width : '50%', height : '30%'}}></Image>
          <Text style={{marginTop : 20, fontSize : 20}}>Maaf Koneksi Anda Terputus!</Text>
          <View style={{flexDirection : 'row', marginTop : 15}}>
            <TouchableOpacity style={{padding : 10, margin : 10, backgroundColor : '#288EDB'}} onPress={() => this.CheckConnectivity()}><Text style={{color : '#FFF', fontSize : 16}}>Reload</Text></TouchableOpacity>
          </View>
        </View>
      </>
    )
  }
}

export default OfflinePage