import React from 'react'
import { 
  Text, 
  Alert, 
  Image, 
  StyleSheet, 
  View, 
  TouchableOpacity, 
  StatusBar, 
  BackHandler
} from 'react-native'
import {Actions} from 'react-native-router-flux'
import {
  Item, 
  Icon, 
  Label, 
  Input
} from 'native-base'
import { ScrollView } from 'react-native-gesture-handler'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay'
import NetInfo from "@react-native-community/netinfo"

class Login extends React.Component {
  constructor(props) {
    super(props)
    this.handleBackButton = this.handleBackButton.bind(this)
  }
  state = {
    email : '',
    password : '',
    icon : "eye-off",
    hiddenPassword : true,
    error : '',
    loading : false,
    validationMessage : {}
  }
  _changeIcon() {
    this.setState(prevState => ({
      icon : prevState.icon === 'eye' ? 'eye-off' : 'eye',
      hiddenPassword : !prevState.hiddenPassword
    }))
  }
  // async storeData(token, id, email, name) {
  //   await AsyncStorage.setItem('token', token)
  //   await AsyncStorage.setItem('id', id)
  //   await AsyncStorage.setItem('email', email)
  //   await AsyncStorage.setItem('name', name)
  // }
  validLogin = () => {
    this.setState({loading : true, error : ''})
    fetch(`${global.server}/login`,{
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body : JSON.stringify({
        email : this.state.email, 
        password : this.state.password,
      }) 
    })
      .then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.status === 400) {
          let message = {}
          responseJson.data.map((data) => {
            console.log(data)
            message[data.field] = data
          })
          console.log(message)
          this.setState({
            loading : false,
            validationMessage : message
          })
        }  
        if (responseJson.status === 401) {
          Alert.alert(
            'Peringatan !',
            responseJson.message, [{
              text: 'OK',
              onPress: () => {
                this.setState({
                  loading : false
                })
              }
            }, ], {
              cancelable: false
            }
          )
        }
        if(responseJson.status === 200) {
          Alert.alert(
            'Selamat',
            responseJson.message, [{
              text: 'OK',
              onPress: () => {
                this.setState({
                  loading : false
                })
              }
            }, ], {
              cancelable: false
            }
          )
        }
        console.log(responseJson)
      })
  } 
  handleBackButton = () => {
    Alert.alert(
      'Exit Event App',
      'Exiting the application?', [{
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => BackHandler.exitApp()
      }, ], {
        cancelable: false
      }
    )
    return true
  } 
  async componentDidMount() {
    this.CheckConnectivity()
  }
  CheckConnectivity = () => {
    const unsubscribe = NetInfo.addEventListener(state => {
      if(state.isConnected !== true) {
        Actions.OfflinePage()
      }
    })
    unsubscribe()
  }
  componentDidCatch() {
    console.log('Ada Catch !')
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton)
  }
  render() {
    return(
      <>  
        <StatusBar backgroundColor="#288EDB" barStyle="light-content" />
        {this.state.loading ? (
          <OrientationLoadingOverlay
            visible={true}
            color="white"
            indicatorSize="large"
            messageFontSize={18}
            message=""
            />
        ) : (<></>)}
        <ScrollView>
          <View>
            <View style={styles.containerImage}>
              <Image source={require('./../../Assets/images/login-logo.png')} style={styles.image} />
            </View>
            <View style={styles.container}>
              {/* {this.state.error ? (
                <View style={styles.alert}>
                  <Text style={{color : '#FFF'}}>{this.state.error}</Text>
                </View>
              ) : (
                <Text></Text>
              )} */}
              <Item floatingLabel style={{borderBottomColor : '#38B6FF'}}>
                <Icon active name='mail' />
                <Label>Email</Label>
                <Input keyboardType="email-address" onChangeText={(email) => this.setState({email : email}) }/>
              </Item>
              {this.state.validationMessage.email != undefined ? (
                <Text style={styles.msgValidation}>{this.state.validationMessage.email.message}</Text>
              ) : (<></>)}
              <Item floatingLabel style={{marginTop : 25, borderBottomColor : '#38B6FF'}}>
                <Icon name="key" />
                <Label>Password</Label>
                <Input secureTextEntry={this.state.hiddenPassword} onChangeText={(password) => this.setState({password : password}) }/>
                <Icon name={this.state.icon} onPress={() => this._changeIcon()} style={{fontSize : 28}}/>
              </Item>
              {this.state.validationMessage.password != undefined ? (
                <Text style={styles.msgValidation}>{this.state.validationMessage.password.message}</Text>
              ) : (<></>)}
              <TouchableOpacity style={[styles.button]} onPress={this.validLogin}>
                <Text style={[styles.text]}>Login</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </>
    )
  }
}
const styles = StyleSheet.create({
  button: {
    display: 'flex',
    height: 45,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop : 45,
    backgroundColor: '#38B6FF',
    shadowColor: '#38B6FF',
    shadowOpacity: 0.4,
    shadowOffset: { height: 10, width: 0 },
    shadowRadius: 20,
  },
  text: {
    fontSize: 16,
    textTransform: 'uppercase',
    color: '#FFFFFF',
  },
  label : {
    marginLeft : 3,
    marginTop : 10
  },
  containerLoading : {
    position : 'absolute',
    top : 0,
    left : 0
  },  
  containerImage : {
    alignItems : 'center',
    marginTop : 50
  },  
  container : {
    marginHorizontal : 10,
    marginTop : 15
  },  
  image : {
    width : 160,
    height : 160
  },
  btn : {
    borderRadius : 30
  },
  footer : {
    marginTop : 50,
    width : '100%',
    alignItems : 'center'
  },
  alert : {
    height : 40,
    alignItems : 'center',
    justifyContent : 'center',
    backgroundColor : '#FF3E30',
    borderRadius : 5,
    marginTop : 10,
    marginBottom : 12
  },
  msgValidation : {
    color : 'red',
    marginTop : 3
  }
})

export default Login