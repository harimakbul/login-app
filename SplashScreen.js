import React from 'react';
import { 
    StyleSheet,
    Image, 
    View,
    StatusBar
} from 'react-native';
class SplashScreen extends React.Component {
    render() {
        return(
            <>
                <StatusBar backgroundColor="#288EDB" barStyle="light-content" />
                <View style={styles.container}>
                    <Image source={require('./Assets/images/splashscreen-logo.png')} style={styles.image} ></Image>
                </View>
            </>
        )
    }
}
const styles = StyleSheet.create({ 
    container : {
        flex : 1,
        alignItems : 'center',
        justifyContent : 'center'
    },  
    image : {
        width : 150,
        height : 150
    }
})
export default SplashScreen;