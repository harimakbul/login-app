import React from 'react';
import { Router, Scene } from 'react-native-router-flux';
import Login from './Component/Login/Login';
import OfflinePage from './OfflinePage'

const Route = () => (
    <Router>
       <Scene key = "root">
          <Scene key = "login" type="reset" component = {Login} hideNavBar={true} initial = {true} />
          <Scene key = "OfflinePage" type="reset" component = {OfflinePage} hideNavBar={true} />
       </Scene>
    </Router>
 )
export default Route;